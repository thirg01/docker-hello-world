FROM node:16-slim
WORKDIR /app
COPY . .
EXPOSE 8080
CMD ["node", "index.js"]
